<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    int[] digits = {1, 2, 3, 4, 5, 6, 7, 8, 9};
%>
<html>
<head>
    <title>Multiplication table</title>
</head>
<body>
<a href="multiplication_table.jsp">Part1</a>
<a href="multiplication_table_jstl.jsp">Part2</a>
<a href="name_input.jsp">Part3</a>
<table border="3">
    <tr>
        <td width="25"></td>
        <td width="25"><%=digits[0]%>
        </td>
        <td width="25"><%=digits[1]%>
        </td>
        <td width="25"><%=digits[2]%>
        </td>
        <td width="25"><%=digits[3]%>
        </td>
        <td width="25"><%=digits[4]%>
        </td>
        <td width="25"><%=digits[5]%>
        </td>
        <td width="25"><%=digits[6]%>
        </td>
        <td width="25"><%=digits[7]%>
        </td>
        <td width="25"><%=digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[0]%>
        </td>
        <td><%=digits[0] * digits[0]%>
        </td>
        <td><%=digits[0] * digits[1]%>
        </td>
        <td><%=digits[0] * digits[2]%>
        </td>
        <td><%=digits[0] * digits[3]%>
        </td>
        <td><%=digits[0] * digits[4]%>
        </td>
        <td><%=digits[0] * digits[5]%>
        </td>
        <td><%=digits[0] * digits[6]%>
        </td>
        <td><%=digits[0] * digits[7]%>
        </td>
        <td><%=digits[0] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[1]%>
        </td>
        <td><%=digits[1] * digits[0]%>
        </td>
        <td><%=digits[1] * digits[1]%>
        </td>
        <td><%=digits[1] * digits[2]%>
        </td>
        <td><%=digits[1] * digits[3]%>
        </td>
        <td><%=digits[1] * digits[4]%>
        </td>
        <td><%=digits[1] * digits[5]%>
        </td>
        <td><%=digits[1] * digits[6]%>
        </td>
        <td><%=digits[1] * digits[7]%>
        </td>
        <td><%=digits[1] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[2]%>
        </td>
        <td><%=digits[2] * digits[0]%>
        </td>
        <td><%=digits[2] * digits[1]%>
        </td>
        <td><%=digits[2] * digits[2]%>
        </td>
        <td><%=digits[2] * digits[3]%>
        </td>
        <td><%=digits[2] * digits[4]%>
        </td>
        <td><%=digits[2] * digits[5]%>
        </td>
        <td><%=digits[2] * digits[6]%>
        </td>
        <td><%=digits[2] * digits[7]%>
        </td>
        <td><%=digits[2] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[3]%>
        </td>
        <td><%=digits[3] * digits[0]%>
        </td>
        <td><%=digits[3] * digits[1]%>
        </td>
        <td><%=digits[3] * digits[2]%>
        </td>
        <td><%=digits[3] * digits[3]%>
        </td>
        <td><%=digits[3] * digits[4]%>
        </td>
        <td><%=digits[3] * digits[5]%>
        </td>
        <td><%=digits[3] * digits[6]%>
        </td>
        <td><%=digits[3] * digits[7]%>
        </td>
        <td><%=digits[3] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[4]%>
        </td>
        <td><%=digits[4] * digits[0]%>
        </td>
        <td><%=digits[4] * digits[1]%>
        </td>
        <td><%=digits[4] * digits[2]%>
        </td>
        <td><%=digits[4] * digits[3]%>
        </td>
        <td><%=digits[4] * digits[4]%>
        </td>
        <td><%=digits[4] * digits[5]%>
        </td>
        <td><%=digits[4] * digits[6]%>
        </td>
        <td><%=digits[4] * digits[7]%>
        </td>
        <td><%=digits[4] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[5]%>
        </td>
        <td><%=digits[5] * digits[0]%>
        </td>
        <td><%=digits[5] * digits[1]%>
        </td>
        <td><%=digits[5] * digits[2]%>
        </td>
        <td><%=digits[5] * digits[3]%>
        </td>
        <td><%=digits[5] * digits[4]%>
        </td>
        <td><%=digits[5] * digits[5]%>
        </td>
        <td><%=digits[5] * digits[6]%>
        </td>
        <td><%=digits[5] * digits[7]%>
        </td>
        <td><%=digits[5] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[6]%>
        </td>
        <td><%=digits[6] * digits[0]%>
        </td>
        <td><%=digits[6] * digits[1]%>
        </td>
        <td><%=digits[6] * digits[2]%>
        </td>
        <td><%=digits[6] * digits[3]%>
        </td>
        <td><%=digits[6] * digits[4]%>
        </td>
        <td><%=digits[6] * digits[5]%>
        </td>
        <td><%=digits[6] * digits[6]%>
        </td>
        <td><%=digits[6] * digits[7]%>
        </td>
        <td><%=digits[6] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[7]%>
        </td>
        <td><%=digits[7] * digits[0]%>
        </td>
        <td><%=digits[7] * digits[1]%>
        </td>
        <td><%=digits[7] * digits[2]%>
        </td>
        <td><%=digits[7] * digits[3]%>
        </td>
        <td><%=digits[7] * digits[4]%>
        </td>
        <td><%=digits[7] * digits[5]%>
        </td>
        <td><%=digits[7] * digits[6]%>
        </td>
        <td><%=digits[7] * digits[7]%>
        </td>
        <td><%=digits[7] * digits[8]%>
        </td>
    </tr>
    <tr>
        <td><%=digits[8]%>
        </td>
        <td><%=digits[8] * digits[0]%>
        </td>
        <td><%=digits[8] * digits[1]%>
        </td>
        <td><%=digits[8] * digits[2]%>
        </td>
        <td><%=digits[8] * digits[3]%>
        </td>
        <td><%=digits[8] * digits[4]%>
        </td>
        <td><%=digits[8] * digits[5]%>
        </td>
        <td><%=digits[8] * digits[6]%>
        </td>
        <td><%=digits[8] * digits[7]%>
        </td>
        <td><%=digits[8] * digits[8]%>
        </td>
    </tr>
</table>
</body>
</html>
