<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %><%
    Object names = session.getAttribute("names");
    if (names != null) {
        ((List<String>) names).clear();
    }
%>

<html>
<head>
    <title>Remove</title>
</head>
<body>
<a href="multiplication_table.jsp">Part1</a>
<a href="multiplication_table_jstl.jsp">Part2</a>
<a href="name_input.jsp">Part3</a>
<h3>List has been removed</h3>
</body>
</html>
