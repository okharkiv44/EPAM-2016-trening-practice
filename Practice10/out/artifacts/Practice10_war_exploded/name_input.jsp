<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<a href="multiplication_table.jsp">Part1</a>
<a href="multiplication_table_jstl.jsp">Part2</a>
<a href="name_input.jsp">Part3</a>
<form action="nameServlet" method="post">
    <input name="name">
    <input type="submit">
</form>
<a href="remove.jsp"><h3>remove</h3></a>
<ul>
    <c:forEach items="${names}" var="name">
        <li>${name}</li>
    </c:forEach>
</ul>
</body>
</html>
