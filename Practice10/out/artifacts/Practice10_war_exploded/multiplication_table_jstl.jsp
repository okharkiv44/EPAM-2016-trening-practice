<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Multiplication table</title>
</head>
<body>
<a href="multiplication_table.jsp">Part1</a>
<a href="multiplication_table_jstl.jsp">Part2</a>
<a href="name_input.jsp">Part3</a>
<table border="3">
    <tr>
        <td width="25"></td>
        <c:forEach var="i" begin="1" end="9">
            <td width="25">${i}
            </td>
        </c:forEach>
    </tr>

    <c:forEach var="i" begin="1" end="9">
        <tr>
            <td width="25">${i}
            </td>
            <c:forEach var="j" begin="1" end="9">
                <td width="25">${i * j}
                </td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
</body>
</html>
