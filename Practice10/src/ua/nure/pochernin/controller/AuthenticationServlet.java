package ua.nure.pochernin.controller;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AuthenticationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // obtain an environment naming context
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            // look up a data source
            DataSource ds = (DataSource) envCtx.lookup("jdbc/practice10");
            // obtain a connection from the pool
            Connection con = ds.getConnection();

            String login = request.getParameter("login");
            String password = request.getParameter("password");
            boolean isAuthenticated = false;

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT login, password FROM users");

            while (rs.next()) {
                if (login.equals(rs.getString(1)) && password.equals(rs.getString(2))) {
                    isAuthenticated = true;
                    break;
                }
            }

            con.close();

            if (isAuthenticated) {
                request.getRequestDispatcher("name_input.jsp").forward(request, response);
            } else {
                response.sendRedirect("authentication.html");
            }
        } catch (NamingException | SQLException ex) {
            // log an exception to a console
            ex.printStackTrace();
            // send an exception to a client
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            response.getWriter().println(sw);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
