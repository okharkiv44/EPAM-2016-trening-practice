package controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class InputServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int x = 0;
        int y = 0;
        String op = request.getParameter("op");
        boolean isValid = true;

        try {
            x = Integer.parseInt(request.getParameter("x"));
            y = Integer.parseInt(request.getParameter("y"));
        } catch (NumberFormatException e) {
            isValid = false;
        }



        if (op == null || !(op.equals("-") || op.equals("+"))) {
            isValid = false;
        }

        PrintWriter writer = response.getWriter();
        writer.write("<!DOCTYPE HTML><html><head><title></title></head><body><p>");

        if (isValid) {
            writer.write(String.valueOf(calculate(x, y, op)));
        } else {
            writer.write("invalid input");
        }

        writer.write("<p><a href=\"calc.html\">calculate</a></p>");
        writer.write("</p></body></html>");
        writer.close();
    }

    private int calculate(int x, int y, String op) {
        int res = 0;
        if (op.equals("-")) {
            res = x - y;
        } else if (op.equals("+")) {
            res = x + y;
        }

        return res;
    }
}
