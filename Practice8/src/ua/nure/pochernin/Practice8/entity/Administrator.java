package ua.nure.pochernin.Practice8.entity;

public class Administrator implements User {
    private int id;
    private String name;

    public Administrator(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
