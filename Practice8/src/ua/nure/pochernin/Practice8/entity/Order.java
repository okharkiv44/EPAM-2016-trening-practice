package ua.nure.pochernin.Practice8.entity;

public class Order {
    private Client client;
    private Administrator admin;
    private int[] menuItems;

    public Order(Client client, Administrator admin, int... menuItems) {
        this.client = client;
        this.admin = admin;
        this.menuItems = menuItems;
    }

    public Client getClient() {
        return client;
    }

    public Administrator getAdmin() {
        return admin;
    }

    public int[] getMenuItems() {
        return menuItems;
    }
}
