package ua.nure.pochernin.Practice8;

import ua.nure.pochernin.Practice8.entity.Administrator;
import ua.nure.pochernin.Practice8.entity.Order;
import ua.nure.pochernin.Practice8.entity.Client;
import ua.nure.pochernin.Practice8.entity.User;
import ua.nure.pochernin.Practice8.util.DBUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Restaurant {
    private static final Logger LOG = Logger.getLogger(Restaurant.class.getName());

    public List<User> findAllUsers() {
        List<User> users = new ArrayList<>();

        try(Connection connection = DBUtils.getConnection()) {
            Statement st = connection.createStatement();
            ResultSet clientsRs = st.executeQuery("SELECT id, name FROM clients");

            while (clientsRs.next()) {
                users.add(new Client(clientsRs.getInt(1), clientsRs.getString(2)));
            }

            ResultSet adminsRs = st.executeQuery("SELECT id, name FROM administrators");

            while (adminsRs.next()) {
                users.add(new Administrator(adminsRs.getInt(1), adminsRs.getString(2)));
            }
        } catch (SQLException e) {
            LOG.warning(e.getMessage());
        }

        return users;
    }

    public Order makeOrder(Client client, Administrator admin, int menuId, int... menuItems) {
        Order order = new Order(client, admin, menuItems);

        try(Connection connection = DBUtils.getConnection()) {
            PreparedStatement orderPst = connection.prepareStatement("INSERT INTO `order` (date, confirmed) VALUES (CURRENT_DATE, 0)", Statement.RETURN_GENERATED_KEYS);
            orderPst.executeUpdate();
            ResultSet rs = orderPst.getResultSet();
            rs.next();
            int orderId = rs.getInt("id");

            System.out.println(orderId);


            PreparedStatement orderMenuPst = connection.prepareStatement("INSERT INTO `order_menu` VALUES (?, ?, ?)");
            for (int item : menuItems) {
                orderMenuPst.setInt(1, orderId);
                orderMenuPst.setInt(2, menuId);
                orderMenuPst.setInt(3, item);

                orderMenuPst.executeUpdate();
            }
        } catch (SQLException e) {
            LOG.warning(e.getMessage());
        }

        return order;
    }
}
