package ua.nure.pochernin.Practice8.util;

import java.sql.*;
import java.util.logging.Logger;

public class DBUtils {
    private static final Logger LOG = Logger.getLogger(DBUtils.class.getName());
    private static final String URL = "jdbc:mysql://localhost:3306/restaurant";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            LOG.warning(e.getMessage());
        }
    }

    public static Connection getConnection() {
        Connection connection = null;

        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            LOG.warning(e.getMessage());
        }

        return connection;
    }
}
