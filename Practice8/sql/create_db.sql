CREATE SCHEMA IF NOT EXISTS restaurant DEFAULT CHARACTER SET utf8 ;
USE restaurant ;

CREATE TABLE IF NOT EXISTS restaurant.`product` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `name` VARCHAR(45) NOT NULL,
  `price` DOUBLE NOT NULL)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS restaurant.menu (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `date` DATE NOT NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS restaurant.`menu_product` (
  `menu_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  PRIMARY KEY (`menu_id`, `product_id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `restaurant`.`clients` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `name` VARCHAR(45) NOT NULL)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `restaurant`.`administrators` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(45) NOT NULL)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS restaurant.`order` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `date` DATE NOT NULL,
  `confirmed` BIT(1) NOT NULL)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS restaurant.`order_menu` (
  `order_id` INT NOT NULL,
  `menu_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  PRIMARY KEY (`order_id`, `menu_id`, `product_id`))
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `restaurant`.`client_order` (
  `client_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`client_id`, `order_id`))
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `restaurant`.`administrator_order` (
  `administrator_id` INT NOT NULL,
  `order_id` INT NOT NULL,
  PRIMARY KEY (`administrator_id`, `order_id`))
  ENGINE = InnoDB;