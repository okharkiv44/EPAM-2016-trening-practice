INSERT INTO `menu` (date) VALUES (CURRENT_DATE);

INSERT INTO `product` (name, price) VALUES ('beer', 5.50);
INSERT INTO `product` (name, price) VALUES ('beef', 15.00);
INSERT INTO `product` (name, price) VALUES ('bread', 2.00);
INSERT INTO `product` (name, price) VALUES ('juice', 5.50);
INSERT INTO `product` (name, price) VALUES ('potato', 6.75);

INSERT INTO `menu_product` VALUES (1, 1);
INSERT INTO `menu_product` VALUES (1, 2);
INSERT INTO `menu_product` VALUES (1, 3);
INSERT INTO `menu_product` VALUES (1, 4);

INSERT INTO `clients` (name) VALUES ('Mike');
INSERT INTO `clients` (name) VALUES ('John');
INSERT INTO `clients` (name) VALUES ('Anna');

INSERT INTO `administrators` (name) VALUES ('Andrew');

INSERT INTO `order` (date, confirmed) VALUES (CURRENT_DATE, 0);

INSERT INTO `order_menu` VALUES (1, 1, 1);
INSERT INTO `order_menu` VALUES (1, 1, 2);
INSERT INTO `order_menu` VALUES (1, 1, 3);

INSERT INTO `client_order` VALUES (1, 1);

INSERT INTO `administrator_order` VALUES (1, 1);